#!/usr/bin/env python
import rospy
import math
from geometry_msgs.msg import PoseStamped, PointStamped, Twist
from tf.transformations import euler_from_quaternion

k = 1   # number of time steps ahead we want to estimate the trajectory
K = 0.75     # control gain
eps = 0.1   # distance to point p
d_f = 0.3   # distance to follower
alpha = math.pi/2  # angle to follower
loop_rate = 10      # rate of main loop
x, y, theta = 0, 0, 0   # position of following robot
x_l, y_l, theta_l, v_l, w_l = 0, 0, 0, 0, 0     # initialize leader pose and control inputs
x_p, y_p, x_p_d, y_p_d, x_p_d_old, y_p_d_old = 0, 0, 0, 0, 0, 0  # initialize variables
vel_msg = Twist()

def leader_pose_callback(pose_msg):    # get pose from motion capture system
    global x_l, y_l, theta_l
    x_l = pose_msg.pose.position.x
    y_l = pose_msg.pose.position.y
    quat = pose_msg.pose.orientation
    (roll_l, pitch_l, theta_l) = euler_from_quaternion([quat.x, quat.y, quat.z, quat.w])

def follower_pose_callback(pose_msg):    # get pose from motion capture system
    global x, y, theta
    x = pose_msg.pose.position.x
    y = pose_msg.pose.position.y
    quat = pose_msg.pose.orientation
    (roll, pitch, theta) = euler_from_quaternion([quat.x, quat.y, quat.z, quat.w])

def cmd_vel_callback(cmd_vel_msg):
    global v_l, w_l
    v_l = cmd_vel_msg.linear.x
    w_l = cmd_vel_msg.angular.z

def publish_p(p_pub):   # publish the point p for rviz visualization
    global x_p, y_p
    x_p = x + eps*math.cos(theta)
    y_p = y + eps*math.sin(theta)

    p_msg = PointStamped()
    p_msg.header.stamp = rospy.Time.now()
    p_msg.header.frame_id = "world"
    p_msg.point.x = x_p
    p_msg.point.y = y_p

    p_pub.publish(p_msg)

def estimate_trajectory(p_d_pub, x_d_pub):
    global x_p_d, y_p_d

    # compute current desired location of follower robot based on the desired distance and relative angle between robots
    x_d = x_l + d_f * math.cos(alpha + theta_l)
    y_d = y_l + d_f * math.sin(alpha + theta_l)

    # publish the current desired position for visualization in rviz
    x_d_msg = PointStamped()
    x_d_msg.header.stamp = rospy.Time.now()
    x_d_msg.header.frame_id = "world"
    x_d_msg.point.x = x_d
    x_d_msg.point.y = y_d
    x_d_pub.publish(x_d_msg)

    # predict trajectory for leader robot in k time steps
    x_l_k = x_l + v_l * math.cos(theta_l) * loop_rate * k
    y_l_k = y_l + v_l * math.sin(theta_l) * loop_rate * k

    # predict trajectory for follower robot in k time steps
    x_d_k = x_l_k + d_f * math.cos(alpha + theta_l)
    y_d_k = y_l_k + d_f * math.sin(alpha + theta_l)

    # estimate tangent of follower trajectory
    tangent = math.atan2(y_d_k - y_d, x_d_k - x_d)

    # compute point p desired
    x_p_d = x_d + eps*math.cos(tangent)
    y_p_d = y_d + eps*math.sin(tangent)

    # publish p desired for visualization in rviz
    p_d_msg = PointStamped()
    p_d_msg.header.stamp = rospy.Time.now()
    p_d_msg.header.frame_id = "world"
    p_d_msg.point.x = x_p_d
    p_d_msg.point.y = y_p_d

    p_d_pub.publish(p_d_msg)

def publish_cmd_vel(cmd_vel_pub):   # compute control signals (translational vel v and rotational vel w)
    global x_p, y_p, x_p_d_old, y_p_d_old, vel_msg

    # compute derivatives
    d_x_p_d = (x_p_d - x_p_d_old)*loop_rate
    d_y_p_d = (y_p_d - y_p_d_old)*loop_rate

    d_x_p = K*(x_p_d - x_p) + d_x_p_d
    d_y_p = K*(y_p_d - y_p) + d_y_p_d

    # comput control inputs
    v = d_x_p * math.cos(theta) + d_y_p * math.sin(theta)
    w = 1.5*(-d_x_p * math.sin(theta) + d_y_p * math.cos(theta)) / eps

    print "v: {}".format(v)
    print "w: {}".format(w)
    #print "theta: {}".format(theta)

    # publish velocity
    vel_msg.linear.x = v
    vel_msg.angular.z = w
    cmd_vel_pub.publish(vel_msg)

    # update variables
    x_p_d_old = x_p_d
    y_p_d_old = y_p_d

def follow_leader():
    # publishers
    cmd_vel_pub = rospy.Publisher('/turtlebot01/cmd_vel', Twist, queue_size=5)
    global cmd_vel_pub
    p_pub = rospy.Publisher('/point_p_f', PointStamped, queue_size=5)
    p_d_pub = rospy.Publisher('/point_p_d_f', PointStamped, queue_size=5)
    x_d_pub = rospy.Publisher('/point_x_d_f', PointStamped, queue_size=5)
    # subscribers
    leader_pose_sub = rospy.Subscriber('/vrpn_client_node/turtlebot00/pose', PoseStamped, leader_pose_callback)
    follower_pose_sub = rospy.Subscriber('/vrpn_client_node/turtlebot01/pose', PoseStamped, follower_pose_callback)
    cmd_vel_sub = rospy.Subscriber('/turtlebot00/cmd_vel', Twist, cmd_vel_callback)

    rospy.on_shutdown(stop)
    rate = rospy.Rate(loop_rate)

    while not rospy.is_shutdown():
        publish_p(p_pub)
        estimate_trajectory(p_d_pub, x_d_pub)
        publish_cmd_vel(cmd_vel_pub)

        rate.sleep()

def stop(): # stops the turtlebot at shutdown
    rospy.loginfo("Stop TurtleBot")
    cmd_vel_pub.publish(Twist()) # a default Twist has linear.x of 0 and angular.z of 0.  So it'll stop TurtleBot
    rospy.sleep(1) # sleep just makes sure TurtleBot receives the stop command prior to shutting down the script

if __name__ == "__main__":
    rospy.init_node('leader_follower_control')
    rospy.loginfo("To stop TurtleBot CTRL + C")
    follow_leader()
