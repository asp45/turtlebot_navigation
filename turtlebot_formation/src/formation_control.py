#!/usr/bin/env python
import rospy
import math
from scipy.special import comb
from tf.transformations import euler_from_quaternion
from std_msgs.msg import Float32MultiArray
from geometry_msgs.msg import PoseStamped, PointStamped, Twist

loop_rate = 10
N = 3   # number of followers
k = 1   # number of time steps for trajectory estimation
x_d = [0, 0, 0]
y_d = [0, 0, 0]
d_f = [0, 0, 0]
x_p = [0, 0, 0]
y_p = [0, 0, 0]
alpha_f = [0, 0, 0]
x_0, y_0, theta_0 = 0, 0, 0

def formation_callback(form_msg):
    global d_f, alpha_f
    d_f[0] = form_msg.data[0]
    alpha_f[0] = form_msg.data[1]
    d_f[1] = form_msg.data[2]
    alpha_f[1] = form_msg.data[3]
    d_f[2] = form_msg.data[4]
    alpha_f[2] = form_msg.data[5]


def cmd_vel_callback(cmd_vel_msg):
    global v_l, w_l
    v_l = cmd_vel_msg.linear.x
    w_l = cmd_vel_msg.angular.z

def pose_00_callback(pose_msg):
    global x_0, y_0, theta_0
    x_0 = pose_msg.pose.position.x
    y_0 = pose_msg.pose.position.y
    quat = pose_msg.pose.orientation
    (roll, pitch, theta_0) = euler_from_quaternion([quat.x, quat.y, quat.z, quat.w])

def pose_01_callback(pose_msg):
    global x_1, y_1, theta_1
    x_1 = pose_msg.pose.position.x
    y_1 = pose_msg.pose.position.y
    quat = pose_msg.pose.orientation
    (roll, pitch, theta_1) = euler_from_quaternion([quat.x, quat.y, quat.z, quat.w])

def pose_02_callback(pose_msg):
    global x_2, y_2, theta_2
    x_2 = pose_msg.pose.position.x
    y_2 = pose_msg.pose.position.y
    quat = pose_msg.pose.orientation
    (roll, pitch, theta_2) = euler_from_quaternion([quat.x, quat.y, quat.z, quat.w])

def pose_03_callback(pose_msg):
    global x_3, y_3, theta_3
    x_3 = pose_msg.pose.position.x
    y_3 = pose_msg.pose.position.y
    quat = pose_msg.pose.orientation
    (roll, pitch, theta_3) = euler_from_quaternion([quat.x, quat.y, quat.z, quat.w])

def estimate_trajectories(): #p_d_pub, x_d_pub):
    global x_p, y_p, x_p_d, y_p_d

    # compute the point p for all follower robots
    x_p[0] = x_1 + eps*math.cos(theta_1)
    y_p[0] = y_1 + eps*math.sin(theta_1)
    x_p[1] = x_2 + eps*math.cos(theta_2)
    y_p[1] = y_2 + eps*math.sin(theta_2)
    x_p[2] = x_3 + eps*math.cos(theta_3)
    y_p[2] = y_3 + eps*math.sin(theta_3)

    # compute current desired location of follower robot based on the desired distance and relative angle between robots
    for i in range(N):
        print "hi"
        x_d[i] = x_0 + d_f[i] * math.cos(alpha_f[i] + theta_0)
        y_d[i] = y_0 + d_f[i] * math.sin(alpha_f[i] + theta_0)

    #print x_d
    print "out"
    print x_d
    # publish the current desired position for visualization in rviz

    # predict trajectory for leader robot in k time steps
    x_0_k = x_0 + v_0 * math.cos(theta_0) * loop_rate * k
    y_0_k = y_0 + v_0 * math.sin(theta_0) * loop_rate * k


'''



    # predict trajectory for follower robot in k time steps
    x_d_k = x_l_k + d_f * math.cos(alpha + theta_l)
    y_d_k = y_l_k + d_f * math.sin(alpha + theta_l)

    # estimate tangent of follower trajectory
    tangent = math.atan2(y_d_k - y_d, x_d_k - x_d)

    # compute point p desired
    x_p_d = x_d + eps*math.cos(tangent)
    y_p_d = y_d + eps*math.sin(tangent)

'''

def formation_control():
    # publishers
    cmd_vel_1_pub = rospy.Publisher('/turtlebot01/cmd_vel', Twist, queue_size=5)
    cmd_vel_2_pub = rospy.Publisher('/turtlebot02/cmd_vel', Twist, queue_size=5)
    cmd_vel_3_pub = rospy.Publisher('/turtlebot03/cmd_vel', Twist, queue_size=5)
    global cmd_vel_1_pub, cmd_vel_2_pub, cmd_vel_3_pub
    # subscribers
    formation_sub = rospy.Subscriber('/formation', Float32MultiArray, formation_callback)
    cmd_vel_sub = rospy.Subscriber('/turtlebot00/cmd_vel', Twist, cmd_vel_callback)
    pose_00_sub = rospy.Subscriber('/vrpn_client_node/turtlebot00/pose', PoseStamped, pose_00_callback)
    pose_01_sub = rospy.Subscriber('/vrpn_client_node/turtlebot01/pose', PoseStamped, pose_01_callback)
    pose_02_sub = rospy.Subscriber('/vrpn_client_node/turtlebot02/pose', PoseStamped, pose_02_callback)
    pose_03_sub = rospy.Subscriber('/vrpn_client_node/turtlebot03/pose', PoseStamped, pose_03_callback)

    rospy.on_shutdown(stop)
    rate = rospy.Rate(loop_rate)

    while not rospy.is_shutdown():
        #publish_p(p_pub)
        estimate_trajectories()       #p_d_pub, x_d_pub)
        #publish_cmd_vel(cmd_vel_pub)

        "What it does:"
        "subscribes to formation"
        "estimate leader trajectory"
        "estimates follower trajectory"
        "computes follower cmd vel"
        "ensures collision avoidance"

        rate.sleep()

def stop(): # stops all turtlebots at shutdown
    rospy.loginfo("Stop TurtleBot")
    cmd_vel_1_pub.publish(Twist()) # a default Twist has linear.x of 0 and angular.z of 0.  So it'll stop TurtleBot
    cmd_vel_2_pub.publish(Twist())
    cmd_vel_3_pub.publish(Twist())
    rospy.sleep(1) # sleep just makes sure TurtleBot receives the stop command prior to shutting down the script

if __name__ == "__main__":
    rospy.init_node('formation_control')
    rospy.loginfo("To stop TurtleBots CTRL + C")
    formation_control()
