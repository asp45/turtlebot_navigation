#!/usr/bin/env python
import rospy
import sys
import math
import matplotlib.pyplot as plt
from std_msgs.msg import Float32MultiArray

d = 0.2     # minimum distance between robots
x_0 = 0     # x position of leader robot
y_0 = 0     # y position of leader robot
alpha = math.pi/2   # angle of leader robot


def plot_formation(form):
    if form == "Triangle":
        print form
        d_1 = d/math.sin(math.pi/4)
        alpha_1 = 3*math.pi/4
        d_2 = d
        alpha_2 = math.pi
        d_3 = d/math.sin(math.pi/4)
        alpha_3 = -3*math.pi/4
    elif form == "Square":
        print form
        d_1 = d
        alpha_1 = math.pi/2
        d_2 = d/math.sin(math.pi/4)
        alpha_2 = 3*math.pi/4
        d_3 = d
        alpha_3 = math.pi
    elif form == "Horizontal":
        print form
        d_1 = d
        alpha_1 = math.pi/2
        d_2 = 2*d
        alpha_2 = math.pi/2
        d_3 = 3*d
        alpha_3 = math.pi/2
    elif form == "Vertical":
        print form
        d_1 = d
        alpha_1 = math.pi
        d_2 = 2*d
        alpha_2 = math.pi
        d_3 = 3*d
        alpha_3 = math.pi

    x_1 = x_0 + d_1 * math.cos(alpha_1 + alpha)
    y_1 = y_0 + d_1 * math.sin(alpha_1 + alpha)
    x_2 = x_0 + d_2 * math.cos(alpha_2 + alpha)
    y_2 = y_0 + d_2 * math.sin(alpha_2 + alpha)
    x_3 = x_0 + d_3 * math.cos(alpha_3 + alpha)
    y_3 = y_0 + d_3 * math.sin(alpha_3 + alpha)

    x = [x_0, x_1, x_2, x_3]
    y = [y_0, y_1, y_2, y_3]

    print x
    print y


    plt.axis([-1, 1, -1, 1])
    plt.plot(x, y, 'o')
    plt.pause(0.0001)


def publish_formation(form):
    if form == "Triangle":
        print form
        d_1 = d/math.sin(math.pi/4)
        alpha_1 = 3*math.pi/4
        d_2 = d
        alpha_2 = math.pi
        d_3 = d/math.sin(math.pi/4)
        alpha_3 = -3*math.pi/4
    elif form == "Square":
        print form
        d_1 = d
        alpha_1 = math.pi/2
        d_2 = d/math.sin(math.pi/4)
        alpha_2 = 3*math.pi/4
        d_3 = d
        alpha_3 = math.pi
    elif form == "Horizontal":
        print form
        d_1 = d
        alpha_1 = math.pi/2
        d_2 = 2*d
        alpha_2 = math.pi/2
        d_3 = 3*d
        alpha_3 = math.pi/2
    elif form == "Vertical":
        print form
        d_1 = d
        alpha_1 = math.pi
        d_2 = 2*d
        alpha_2 = math.pi
        d_3 = 3*d
        alpha_3 = math.pi

    form_pub = rospy.Publisher('/formation', Float32MultiArray, queue_size=5)
    array = Float32MultiArray()

    array.data.append(d_1)
    array.data.append(alpha_1)
    array.data.append(d_2)
    array.data.append(alpha_2)
    array.data.append(d_3)
    array.data.append(alpha_3)
    print array
    print array.data

    rate = rospy.Rate(10)

    while not rospy.is_shutdown():

        form_pub.publish(array)

        rate.sleep()




if __name__ == "__main__":
    rospy.init_node('formation_publisher')
    args = rospy.myargv(argv = sys.argv)    # reads input argument of which formation publish

    formations = ["Triangle", "Square", "Horizontal", "Vertical"]    # implemented formations: Triangle, Square, Horizontal, Vertical
    form = args[1]
    if (form not in formations):
        rospy.logwarn("Formation is invalid. Set one of the following formations: Triangle, Square, Horizontal, Vertical.")
    else:
        rospy.loginfo("Publishing formation")
        plot_formation(form)
        publish_formation(form)
