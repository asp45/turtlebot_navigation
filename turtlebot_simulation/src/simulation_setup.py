#!/usr/bin/env python
import rospy
import math
from geometry_msgs.msg import PoseStamped, Twist
from nav_msgs.msg import Odometry

pos_00_msg = PoseStamped()
pos_01_msg = PoseStamped()
pos_02_msg = PoseStamped()
pos_03_msg = PoseStamped()

def pose_00_callback(pose_msg):
    global pos_00_msg
    pos_00_msg.header = pose_msg.header
    pos_00_msg.pose = pose_msg.pose.pose

def pose_01_callback(pose_msg):
    global pos_01_msg
    pos_01_msg.header = pose_msg.header
    pos_01_msg.pose = pose_msg.pose.pose

def pose_02_callback(pose_msg):
    global pos_02_msg
    pos_02_msg.header = pose_msg.header
    pos_02_msg.pose = pose_msg.pose.pose

def pose_03_callback(pose_msg):
    global pos_03_msg
    pos_03_msg.header = pose_msg.header
    pos_03_msg.pose = pose_msg.pose.pose


if __name__ == "__main__":
    rospy.init_node('simulation_setup')

    ### publishers ###
    pos_00_pub = rospy.Publisher('/vrpn_client_node/turtlebot00/pose', PoseStamped, queue_size=5)
    pos_01_pub = rospy.Publisher('/vrpn_client_node/turtlebot01/pose', PoseStamped, queue_size=5)
    pos_02_pub = rospy.Publisher('/vrpn_client_node/turtlebot02/pose', PoseStamped, queue_size=5)
    pos_03_pub = rospy.Publisher('/vrpn_client_node/turtlebot03/pose', PoseStamped, queue_size=5)

    ### subscribers ###
    pose_00_sub = rospy.Subscriber('/turtlebot00/odom', Odometry, pose_00_callback)
    pose_01_sub = rospy.Subscriber('/turtlebot01/odom', Odometry, pose_01_callback)
    pose_02_sub = rospy.Subscriber('/turtlebot02/odom', Odometry, pose_02_callback)
    pose_03_sub = rospy.Subscriber('/turtlebot03/odom', Odometry, pose_03_callback)

    rate = rospy.Rate(10)

    while not rospy.is_shutdown():
        pos_00_pub.publish(pos_00_msg)
        pos_01_pub.publish(pos_01_msg)
        pos_02_pub.publish(pos_02_msg)
        pos_03_pub.publish(pos_03_msg)

        rate.sleep()
