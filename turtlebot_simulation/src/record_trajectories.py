#!/usr/bin/env python
import rospy
from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import PointStamped, PoseStamped, PoseWithCovarianceStamped

### This script records data from the desired trajectory, the motion capture system
### and the monte carlo localization and saves them in the file trajectories.txt.

x, y, theta = 0, 0, 0
x_amcl, y_amcl, theta_amcl = 0, 0, 0
x_d, y_d = 0, 0
got_data = False


def pose_callback(pose_msg):    # get pose from motion capture system
    global x, y, theta
    x = pose_msg.pose.position.x
    y = pose_msg.pose.position.y
    quat = pose_msg.pose.orientation
    (roll, pitch, theta) = euler_from_quaternion([quat.x, quat.y, quat.z, quat.w])

def amcl_pose_callback(pose_msg):    # get pose from monte carlo localization
    global x_amcl, y_amcl, theta_amcl
    x_amcl = pose_msg.pose.pose.position.x
    y_amcl = pose_msg.pose.pose.position.y
    quat = pose_msg.pose.pose.orientation
    (roll, pitch, theta_amcl) = euler_from_quaternion([quat.x, quat.y, quat.z, quat.w])

def traj_callback(traj_msg):    # get desired x and y position from trajectory publisher
    global x_d, y_d, got_data
    x_d = traj_msg.point.x
    y_d = traj_msg.point.y
    got_data = True

def record():

    file = "/home/fred/turtlebot_ws/src/turtlebot_navigation/turtlebot_simulation/trajectories/trajectories.txt"

    # subscribers
    pose_sub = rospy.Subscriber('/vrpn_client_node/turtlebot00/pose', PoseStamped, pose_callback)
    amcl_pose_sub = rospy.Subscriber('/amcl_pose', PoseWithCovarianceStamped, amcl_pose_callback)
    traj_sub = rospy.Subscriber('/trajectory', PointStamped, traj_callback)

    with open(file, "a") as f:
        f.write("# {} {} {} {} {} {} {} {} {}\n".format("desired x", "desired y", "motive x", "motive y", "motive theta", "amcl x", "amcl y", "amcl theta", "time"))

    starttime = rospy.get_time()
    rospy.on_shutdown(stop_recording)
    rate = rospy.Rate(5)

    while not rospy.is_shutdown():
        time = rospy.get_time() - starttime
        if got_data:
            with open(file, "a") as f:
                f.write("{} {} {} {} {} {} {} {} {}\n".format(x_d, y_d, x, y, theta, x_amcl, y_amcl, theta_amcl, time))

        rate.sleep()

def stop_recording():
    rospy.loginfo("Stop recording")


if __name__ == "__main__":
    rospy.init_node('trajectory_recorder')
    rospy.loginfo("Recording trajectories...")
    record()
