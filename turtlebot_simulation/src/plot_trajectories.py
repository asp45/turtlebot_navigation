#!/usr/bin/env python
import rospy
import matplotlib.pyplot as plt

def plot():
    x_d = []
    y_d = []
    x = []
    y = []
    theta = []
    x_amcl = []
    y_amcl = []
    theta_amcl = []
    time = []

    with open("/home/fred/turtlebot_ws/src/turtlebot_navigation/turtlebot_simulation/trajectories/trajectories.txt", "r") as f:
        data = f.readlines()
        for lines in data:
            line = lines.split(" ")
            if line[0] != '#':
                x_d.append(float(line[0]))
                y_d.append(float(line[1]))
                x.append(float(line[2]))
                y.append(float(line[3]))
                theta.append(float(line[4]))
                x_amcl.append(float(line[5]))
                y_amcl.append(float(line[6]))
                theta_amcl.append(float(line[7]))
                time.append(float(line[8]))

    plt.plot(x, y, 'r', label="Motion capture")
    plt.plot(x_d,y_d, 'b', label="Trajectory" )
    plt.plot(x_amcl, y_amcl, label="AMCL")

    #plt.axis([0, 2, 0, 2])
    plt.legend(loc=1)

    plt.show()

if __name__ == "__main__":
    rospy.init_node('trajectory_plotter')
    rospy.loginfo("Plotting trajectories...")
    plot()
