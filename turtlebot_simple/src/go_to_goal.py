#!/usr/bin/env python
import rospy
import math
from geometry_msgs.msg import Twist
from geometry_msgs.msg import PoseStamped
from tf.transformations import euler_from_quaternion

k_l = 0.4   # linear constant
k_a = 4     # angular constant
x, y, theta = 0, 0, 0   # initialize pose_msg

def pose_callback(pose_msg):    # get pose from motion capture system
    global x, y, theta
    pose = pose_msg.pose
    x = pose.position.x
    y = pose.position.y
    quat = pose.orientation
    (roll, pitch, theta) = euler_from_quaternion([quat.x, quat.y, quat.z, quat.w])

def move():
    cmd_vel_pub = rospy.Publisher('/turtlebot00/cmd_vel', Twist, queue_size=5)
    pose_sub = rospy.Subscriber('/vrpn_client_node/turtlebot00/pose', PoseStamped, pose_callback)
    vel_msg = Twist()

    ### set goal position ###
    x_goal = 0
    y_goal = 0

    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        error_theta = math.atan2(y_goal - y, x_goal - x) - theta
        # ensure that the error is within the range [-pi, pi]
        if error_theta < -math.pi:
            error_theta += 2*math.pi
        elif error_theta >= math.pi:
            error_theta -= 2*math.pi
        error_dist = math.sqrt((x_goal - x)**2 + (y_goal - y)**2)
        print "error dist: {}".format(error_dist)
        print "error_theta: {}".format(error_theta)
        if error_dist < 0.05:
            print "Goal reached!"
            vel_msg.linear.x = 0.0
            vel_msg.angular.z = 0.0
        else:
            u = k_l * error_dist
            if u > 0.2:
                u = 0.2
            w = k_a * error_theta
            print "u: {}".format(u)
            print "w: {}".format(w)

            vel_msg.linear.x = u
            vel_msg.angular.z = w

        cmd_vel_pub.publish(vel_msg)
        rate.sleep()

if __name__ == "__main__":
    rospy.init_node('cmd_vel_publisher')
    rospy.loginfo("To stop TurtleBot CTRL + C")
    move()
