#!/usr/bin/env python

import rospy
import datetime
from std_msgs.msg import String

def publisher():
    rospy.init_node('publish_time', anonymous=True)
    pub = rospy.Publisher('date_time', String, queue_size=1)
    rate = rospy.Rate(15)
    while not rospy.is_shutdown():
        now = rospy.get_rostime().to_sec()
        date_time = datetime.datetime.fromtimestamp(int(now)).strftime('%Y%m%d %H:%M:%S')
        print date_time
        pub.publish(date_time)
        rate.sleep()


if __name__ == '__main__':
    publisher()
