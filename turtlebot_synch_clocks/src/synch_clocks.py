#!/usr/bin/env python

import rospy
from std_msgs.msg import String
from subprocess import call
from time import sleep

set_date = 0

def datetime_callback(date_msg):
	date = date_msg.data
	global set_date
	if set_date == 0:
		set_date = set_datetime(date)

def set_datetime(date):
	call(['sudo date +%F%T -s "{}"'.format(date)], shell = True)
	return 1

def listener():
	rospy.init_node('get_time', anonymous=True)
	sleep(2)
	rospy.Subscriber("date_time", String, datetime_callback)
	rospy.spin()

if __name__ == '__main__':
	listener()
