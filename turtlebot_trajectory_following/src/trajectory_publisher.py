#!/usr/bin/env python
import rospy
import numpy as np
import sys
import matplotlib.pyplot as plt
from geometry_msgs.msg import PointStamped, PoseStamped
from nav_msgs.msg import Path
from tf.transformations import quaternion_from_euler

r = 1
d = 1.2 # moves the trajectory in to the positive xy- quadrant
delta = 0.001   # to compute the tangent of a point
v = 0.1 # desired velocity
num_wp = 10 # number of waypoints that should be sent to waypoint_navigation
wp_msg = Path()

def plot_trajectory(traj):  # plots the trajectory
    t = np.linspace(0, 2*np.pi, num=1000)

    if traj == "Circle":
        x = r * np.cos(t) + d
        y = r * np.sin(t) + d
    elif traj == "Lemniscare":
        x = np.cos(t) / (np.sin(t)**2 + r) + d
        y = np.cos(t) * np.sin(t) / (np.sin(t)**2 + r) + d
    elif traj == "Rose":
        x = np.cos(2*t)*np.cos(t) + d
        y = np.cos(2*t)*np.sin(t) + d
    else:
        x = 0
        y = 0

    plt.plot(x, y)
    plt.pause(0.0001)

def publish_waypoints(traj):
    global wp_msg
    wp_msg.header.stamp = rospy.Time.now()
    wp_msg.header.frame_id = "map"
    points = np.linspace(0, 2*np.pi, num=num_wp)
    for n in points:
        if traj == "Circle":
            x = r * np.cos(n) + d
            y = r * np.sin(n) + d
            x_1 = r * np.cos(n + delta) + d
            y_1 = r * np.sin(n + delta) + d
        elif traj == "Lemniscare":
            x = np.cos(n) / (np.sin(n)**2 + r) + d
            y = np.cos(n) * np.sin(n) / (np.sin(n)**2 + r) + d
            x_1 = np.cos(n + delta) / (np.sin(n + delta)**2 + r) + d
            y_1 = np.cos(n + delta) * np.sin(n + delta) / (np.sin(n + delta)**2 + r) + d
        elif traj == "Rose":
            x = np.cos(2*n)*np.cos(n) + d
            y = np.cos(2*n)*np.sin(n) + d
            x_1 = np.cos(2*(n + delta))*np.cos(n + delta) + d
            y_1 = np.cos(2*(n + delta))*np.sin(n + delta) + d
        else:
            x = 0
            y = 0

        theta = np.arctan2(y_1 - y, x_1 - x)    # compute angle of rangent

        wp = PoseStamped()
        wp.header.stamp = rospy.Time.now()
        wp.header.frame_id = "map"
        wp.pose.position.x = x
        wp.pose.position.y = y
        wp.pose.position.z = 0
        quat = quaternion_from_euler(0,0,theta)
        wp.pose.orientation.x = quat[0]
        wp.pose.orientation.y = quat[1]
        wp.pose.orientation.z = quat[2]
        wp.pose.orientation.w = quat[3]
        wp_msg.poses.append(wp)


def publish_trajectory(traj):   # publishes the current desired x and y positions on the trajectory
    wp_pub = rospy.Publisher('/waypoints', Path, queue_size=5)
    traj_pub = rospy.Publisher('/trajectory', PointStamped, queue_size=5)
    traj_pub_map = rospy.Publisher('/trajectory_map', PointStamped, queue_size=5)
    traj_msg = PointStamped()
    traj_msg_map = PointStamped()

    starttime = rospy.get_time()
    rate = rospy.Rate(10)

    while not rospy.is_shutdown():

        t = rospy.get_time() - starttime
        i = t * v

        if traj == "Circle":
            x = r * np.cos(i) + d
            y = r * np.sin(i) + d
        elif traj == "Lemniscare":
            x = np.cos(i) / (np.sin(i)**2 + r) + d
            y = np.cos(i) * np.sin(i) / (np.sin(i)**2 + r) + d
        elif traj == "Rose":
            x = np.cos(2*i)*np.cos(i) + d
            y = np.cos(2*i)*np.sin(i) + d
        else:
            x = 0
            y = 0

        # create the message for publishing trajectory data
        traj_msg.header.stamp = rospy.Time.now()
        traj_msg.header.frame_id = "world"
        traj_msg.point.x = x
        traj_msg.point.y = y

        # create another message for publishing trajectory data in map frame (compatible with ROS navigation stack)
        traj_msg_map.header.stamp = rospy.Time.now()
        traj_msg_map.header.frame_id = "map"
        traj_msg_map.point.x = x
        traj_msg_map.point.y = y

        wp_pub.publish(wp_msg)
        traj_pub.publish(traj_msg)
        traj_pub_map.publish(traj_msg_map)

        rate.sleep()


if __name__ == "__main__":
    rospy.init_node('trajectory_publisher')
    args = rospy.myargv(argv = sys.argv)    # reads input argument of which trajectory publish

    trajectories = ["Circle", "Lemniscare", "Rose"]    # implemented trajectories: circle, lemniscare, rose
    traj = args[1]
    if (traj not in trajectories):
        rospy.logwarn("Trajectory is invalid. Set one of the following trajectories: Circle, Lemniscare or Rose.")
    else:
        rospy.loginfo("Publishing trajectory")
        plot_trajectory(traj)
        publish_waypoints(traj)
        publish_trajectory(traj)
