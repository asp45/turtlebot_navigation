#!/usr/bin/env python
import rospy
import math
from geometry_msgs.msg import PoseStamped, PointStamped, Twist, PoseWithCovarianceStamped
from tf.transformations import euler_from_quaternion

K = 0.75     # control gain
eps = 0.1   # distance to point p
loop_rate = 10
x, y, theta = 0, 0, 0   # initialize pose
x_p, y_p, x_d, y_d, x_d_old, y_d_old = 0, 0, 0, 0, 0, 0
vel_msg = Twist()

def pose_callback(pose_msg):    # get pose from motion capture system
    global x, y, theta
    x = pose_msg.pose.pose.position.x
    y = pose_msg.pose.pose.position.y
    quat = pose_msg.pose.pose.orientation
    (roll, pitch, theta) = euler_from_quaternion([quat.x, quat.y, quat.z, quat.w])

def traj_callback(traj_msg):    # get desired x and y position from trajectory publisher
    global x_d, y_d
    x_d = traj_msg.point.x
    y_d = traj_msg.point.y
    #print "x_d: {}".format(x_d)
    #print "y_d: {}".format(y_d)

def publish_p(p_pub):   # publish the point p for rviz visualization
    global x_p, y_p
    x_p = x + eps*math.cos(theta)
    y_p = y + eps*math.sin(theta)

    p_msg = PointStamped()
    p_msg.header.stamp = rospy.Time.now()
    p_msg.header.frame_id = "world"
    p_msg.point.x = x_p
    p_msg.point.y = y_p

    p_pub.publish(p_msg)

def publish_cmd_vel(cmd_vel_pub):   # compute control signals (translational vel v and rotational vel w)
    global x_p, y_p, x_d, y_d, x_d_old, y_d_old, vel_msg

    # compute derivatives
    d_x_d = (x_d - x_d_old)*loop_rate
    d_y_d = (y_d - y_d_old)*loop_rate

    d_x_p = K*(x_d - x_p) + d_x_d
    d_y_p = K*(y_d - y_p) + d_y_d

    # comput control inputs
    v = d_x_p * math.cos(theta) + d_y_p * math.sin(theta)
    w = 1.5*(-d_x_p * math.sin(theta) + d_y_p * math.cos(theta)) / eps

    print "v: {}".format(v)
    print "w: {}".format(w)
    print "theta: {}".format(theta)

    # publish velocity
    vel_msg.linear.x = v
    vel_msg.angular.z = w
    cmd_vel_pub.publish(vel_msg)

    # update variables
    x_d_old = x_d
    y_d_old = y_d

def move():
    # publishers
    cmd_vel_pub = rospy.Publisher('/turtlebot00/cmd_vel', Twist, queue_size=5)
    global cmd_vel_pub
    p_pub = rospy.Publisher('/point_p', PointStamped, queue_size=5)
    # subscribers
    pose_sub = rospy.Subscriber('/amcl_pose', PoseWithCovarianceStamped, pose_callback)
    traj_sub = rospy.Subscriber('/trajectory_map', PointStamped, traj_callback)

    rospy.on_shutdown(stop)
    rate = rospy.Rate(loop_rate)

    while not rospy.is_shutdown():
        publish_p(p_pub)
        publish_cmd_vel(cmd_vel_pub)

        rate.sleep()

def stop(): # stops the turtlebot at shutdown
    rospy.loginfo("Stop TurtleBot")
    cmd_vel_pub.publish(Twist()) # a default Twist has linear.x of 0 and angular.z of 0.  So it'll stop TurtleBot
    rospy.sleep(1) # sleep just makes sure TurtleBot receives the stop command prior to shutting down the script


if __name__ == "__main__":
    rospy.init_node('trajectory_tracker_single_robot')
    rospy.loginfo("To stop TurtleBot CTRL + C")
    move()
