#!/usr/bin/env python
import rospy
import math
import actionlib
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from actionlib_msgs.msg import GoalStatus
from geometry_msgs.msg import Pose, Point, Quaternion
from nav_msgs.msg import Path
from tf.transformations import quaternion_from_euler

client = actionlib.SimpleActionClient('/move_base', MoveBaseAction)
wp_list = []
set_wp = False

def feedback_callback(feedback):
    print('[Feedback] Going to Goal Pose...')

def wp_callback(wp_msg):
    global wp_list, set_wp
    if not set_wp:
        wp_list = wp_msg.poses
        set_wp = True

def follow_trajectory():
    global client
    traj_sub = rospy.Subscriber('/waypoints', Path, wp_callback)
    rospy.on_shutdown(shutdown)

    client.wait_for_server()    # waits until server is started and is listening to goals

    goal = MoveBaseGoal()
    rate = rospy.Rate(10)

    while not rospy.is_shutdown():
        for goal.target_pose in wp_list:
            #goal.target_pose = wp_list[0]
            print goal
            client.send_goal(goal, feedback_cb=feedback_callback)
            client.wait_for_result()
            print('[Result] State: %d'%(client.get_state()))


def shutdown():
    rospy.loginfo("Stop TurtleBot")
    client.cancel_goal()
    cmd_vel_pub = rospy.Publisher('/turtlebot00/cmd_vel', Twist, queue_size=1)
    cmd_vel_pub.publish(Twist()) # a default Twist has linear.x of 0 and angular.z of 0.  So it'll stop TurtleBot
    rospy.sleep(1) # sleep just makes sure TurtleBot receives the stop command prior to shutting down the script


if __name__ == '__main__':
    rospy.init_node('trajectory_following_with_waypoints')
    rospy.loginfo("To stop TurtleBot CTRL + C")
    try:
        follow_trajectory()
    except rospy.ROSInterruptException:
        rospy.loginfo("Navigation finished.")
